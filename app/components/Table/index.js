import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import SearchAppBar from './Search';
import Filter from '../../icons/filter.png';
import PaginationLink from './Pagegination';
import Tabs from './Tabs';

const columns = [
  { id: 'name', label: 'Entity Identified', minWidth: 170 },
  { id: 'code', label: 'Residency', minWidth: 100 },
  {
    id: 'Accept',
    label: 'Accept',
    minWidth: 170,
    align: 'right',
    format: value => value.toLocaleString('en-US'),
  },
  {
    id: 'Deny',
    label: 'Deny',
    minWidth: 170,
    align: 'right',
    format: value => value.toLocaleString('en-US'),
  },
  {
    id: 'When',
    label: 'When',
    minWidth: 170,
    align: 'right',
    format: value => value.toFixed('en-US'),
  },
];

function createData(name, code, Accept, Deny, When) {
  return { name, code, Accept, Deny, When };
}

const rows = [
  createData(
    '10.255.255.255',
    'CA',
    'Essential+ Analytics and C...',
    'NA',
    '2 hours ago',
  ),
  createData(
    '10.255.255.255',
    'CN',
    'Essential + Social Netwo... + 3',
    'NA',
    '20 hours ago',
  ),
  createData('10.255.255.255', 'NY', 'Essential', 'Social Networking'),
  createData('10.255.255.255', 'US', 'All', 'Preformance and Funct...'),
  createData(
    '10.255.255.255',
    'CA',
    'Essential+ Analytics and C... + 2',
    'NA',
    '3 hours ago',
  ),
  createData(
    '10.255.255.255',
    'AU',
    'Essential',
    'NA',
    '20 mins ago',
    '12 hours ago',
  ),
  createData('10.255.255.255', 'DE', 'Essential', 'NA', '16 hours ago'),
  createData('10.255.255.255', 'IE', 'All', 'NA', '18 hours ago'),
  createData('10.255.255.255', 'MX', 'All', 'NA', '24 hours ago'),
  createData('10.255.255.255', 'JP', 'All', 'NA', '1 day ago'),
  createData('10.255.255.255', 'FR', 'Essential', 'NA', '1 day ago'),
  createData('10.255.255.255', 'GB', 'Essential', 'NA', '1 day ago'),
  createData('10.255.255.255', 'RU', 'All', 'NA', '1 day ago'),
  createData('10.255.255.255', 'NG', 'Essential', 'Advertising', '2 day ago'),
  createData('10.255.255.255', 'BR', 'All', 'NA', '3 day ago'),
];

const useStyles = makeStyles({
  root: {
    width: '100%',
    border: '1px solid #E1E4E8',
    borderRadius: '8px',
    marginTop: '35px',
    color: '#24292E',
    fontWeight: '500',
    fontSize: '14px',
    lineHeight: '20px',
  },
  container: {
    maxHeight: 440,
    color: '#24292E',
  },
});

export default function StickyHeadTable() {
  const classes = useStyles();
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(8);

  return (
    <Paper className={classes.root}>
      <div className="header-list">
        <Tabs />
      </div>
      <TableContainer className={classes.container}>
        <div className="filter-section">
          <div className="search-box">
            <SearchAppBar />
          </div>
          <div className="short-box">
            <div>
              <p className="sort-by">SORT BY</p>
            </div>

            <div>
              <select
                name="select-sort"
                id="select-sort"
                className="select-sort sort-text"
              >
                <optgroup label="Sort By">
                  <option value="1">Last A to Z</option>
                  <option value="2"> Last Z to A</option>
                </optgroup>
              </select>
            </div>
            <div className="filtr-btn">
              <button>
                <img src={Filter} alt="filter" />
                filter
              </button>
            </div>
          </div>
        </div>
        <div className="list_description">
          <p>Showing 1-2 of 18 consent list</p>
        </div>
        <Table stickyHeader aria-label="sticky table">
          <TableHead>
            <TableRow>
              {columns.map(column => (
                <TableCell
                  key={column.id}
                  align={column.align}
                  style={{ minWidth: column.minWidth }}
                >
                  {column.label}
                </TableCell>
              ))}
            </TableRow>
          </TableHead>
          <TableBody>
            {rows
              .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
              .map(row => (
                <TableRow hover role="checkbox" tabIndex={-1} key={row.code}>
                  {columns.map(column => {
                    const value = row[column.id];
                    return (
                      <TableCell key={column.id} align={column.align}>
                        {column.format && typeof value === 'number'
                          ? column.format(value)
                          : value}
                      </TableCell>
                    );
                  })}
                </TableRow>
              ))}
          </TableBody>
        </Table>
      </TableContainer>
      <div className="page-nation">
        <PaginationLink />
      </div>
    </Paper>
  );
}
