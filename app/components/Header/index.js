import React from 'react';
import { makeStyles, alpha } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import SearchIcon from '@material-ui/icons/Search';
import InputBase from '@material-ui/core/InputBase';
import logo from '../../icons/Logo.png';
import vector from '../../icons/Vector.png';
import arow from '../../icons/Darrow.png';
import warning from '../../icons/Warning.png';
import setting from '../../icons/setting.png';
import bell from '../../icons/bellSimple.png';
import question from '../../icons/Question.png';
import avator from '../../icons/avator.png';

import '../../stylesheet/header.scss';

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
  },
  search: {
    position: 'relative',
    borderRadius: theme.shape.borderRadius,
    backgroundColor: alpha(theme.palette.common.white, 0.15),
    '&:hover': {
      backgroundColor: alpha(theme.palette.common.white, 0.25),
    },
    marginLeft: 0,
    width: '250px',
    [theme.breakpoints.up('sm')]: {
      marginLeft: theme.spacing(1),
      width: 'auto',
    },
  },
  searchIcon: {
    padding: theme.spacing(0, 2),
    height: '100%',
    position: 'absolute',
    pointerEvents: 'none',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    color: 'white',
  },
  inputRoot: {
    color: 'inherit',
  },
  inputInput: {
    padding: theme.spacing(1, 1, 1, 0),
    // vertical padding + font size from searchIcon
    paddingLeft: `calc(1em + ${theme.spacing(4)}px)`,
    transition: theme.transitions.create('width'),
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      width: '250px',
    },
  },
}));

export function ButtonAppBar() {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <AppBar position="static">
        <Toolbar>
          <IconButton
            edge="start"
            className={classes.menuButton}
            color="inherit"
            aria-label="menu"
          />
          <Typography>
            <img className="logo-icon" src={logo} alt="logo" />
          </Typography>
          <div className="parent-col">
            <div className="nav-bar-left">
              <Typography className="vector">
                <img src={vector} alt="logo" />
              </Typography>
              <Typography className="vector">
                <span>Dashboard</span>
              </Typography>
              <Typography className="vector_nav">
                <span> Data Sources</span>
              </Typography>{' '}
              <Typography className="vector">
                <span>
                  {' '}
                  Playbook
                  <img src={arow} alt="down" />
                </span>
              </Typography>{' '}
              <Typography className="vector">
                insights
                <img src={arow} alt="down" />
              </Typography>{' '}
              <Typography className="vector">
                <span>
                  {' '}
                  Privacy ops
                  <img src={arow} alt="down" />
                </span>
              </Typography>
            </div>
            <div className="nav-bar-right">
              <Toolbar>
                <IconButton
                  edge="start"
                  className={classes.menuButton}
                  color="inherit"
                  aria-label="open drawer"
                />
                <div className={classes.search}>
                  <div className={classes.searchIcon}>
                    <SearchIcon />
                  </div>
                  <InputBase
                    placeholder="Search"
                    classes={{
                      root: classes.inputRoot,
                      input: classes.inputInput,
                    }}
                    inputProps={{ 'aria-label': 'search' }}
                  />
                </div>
                <Typography className="vector-s">
                  <span>
                    <img src={warning} alt="down" />
                  </span>
                  <span>
                    <img src={setting} alt="down" />
                  </span>{' '}
                  <span>
                    <img src={bell} alt="down" />
                  </span>{' '}
                  <span>
                    <img src={question} alt="down" />
                  </span>
                  <span>
                    <img src={avator} alt="down" />
                  </span>
                </Typography>
              </Toolbar>
            </div>
          </div>
        </Toolbar>
      </AppBar>
    </div>
  );
}
