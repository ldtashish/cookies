import React from 'react';
import arrowRight from '../../icons/arrowRight.png';
import '../../stylesheet/heroheader.scss';
import Table from '../Table/index';
import { Link } from 'react-router-dom';

export default function HeroHeader() {
  return (
    <>
      <div className="hero_header">
        <div className="hero-nav">
          <ul className="item-list">
            <Link>
              <li>
                Privacy Ops{' '}
                <span>
                  <img className="icon-arrow" src={arrowRight} alt="arow" />
                </span>
              </li>
            </Link>
            <Link>
              <li>
                Cookies Consent
                <span>
                  <img className="icon-arrow" src={arrowRight} alt="arow" />
                </span>
              </li>
            </Link>
            <li className="active-clr">Domains</li>
            <Link />
          </ul>
        </div>
        <div className="domains">
          <h2 className="heading">Domains</h2>
        </div>
        <div className="col-2">
          <div className="btn-beam">
            <button className="btn">lightbeam.com</button>
          </div>
          <div className="btn-beam">
            <button className="btn">Configure New Domain</button>
          </div>
        </div>
        <Table />
      </div>
    </>
  );
}
