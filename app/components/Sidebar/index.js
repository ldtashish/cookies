import React from 'react';
import '../../stylesheet/sidebar.scss';
import { Link } from 'react-router-dom';
import first from '../../icons/01.png';
import cooki from '../../icons/02.png';
import universal from '../../icons/03.png';
import roap from '../../icons/04.png';
import HeroHeader from '../Heroheader/index';

export function Sidebar() {
  return (
    <>
      <div className="parent-sec">
        <div className="side-bar">
          <div className="rect-one">
            <Link className="dsr">
              <img src={first} alt="cert" className="icon" />
              <p className="icn-text">DSR</p>
            </Link>
            <Link className="dsr active-line">
              <img src={cooki} alt="cert" className="icon" />
              <p className="icn-text active-text"> cookie consent</p>
            </Link>{' '}
            <Link className="dsr">
              <img src={universal} alt="cert" className="icon" />
              <p className="icn-text">Universal Consent</p>
            </Link>
            <Link className="dsr">
              <img src={roap} alt="cert" className="icon" />
              <p className="icn-text">ROPA</p>
            </Link>
          </div>
        </div>
        <div className="hero-bar">
          <HeroHeader />
        </div>
      </div>
    </>
  );
}
